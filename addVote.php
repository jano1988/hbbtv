<?php

/*
 * Leemos el fichero donde tenemos toda la información y seleccionamos el item en la posición que nos indica la variable
 * index pasada como parámetro POST y aumentamos en un uno los votos de este ítem.
 */

$json_data = file_get_contents('data.json');
$songs = json_decode($json_data);

$songs->data[$_POST['index']]->votos = $songs->data[$_POST['index']]->votos + 1;

$fp = fopen('data.json', 'w');
fwrite($fp, json_encode($songs));
fclose($fp);

echo $songs->data[$_POST['index']]->votos;

?>