<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

?>
<!DOCTYPE html PUBLIC "-//HbbTV//1.1.1//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Práctica 1 - HbbTV</title>
    <meta http-equiv="content-type" content="application/vnd.hbbtv.xhtml+xml" charset="UTF-8" />
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="./js/keycodes.js"></script>
    <script type="text/javascript" src="./js/app.js"></script>
</head>

<body onload="hbbtvlib_app_initialize(); mostrarInfo(0);">
    <object id="oipfAppMan" type="application/oipfApplicationManager"></object>
    <div id="background"></div>
    <div id="fullscreen" >
        <video num="-1" id="real" class="noFullscreen" xmlns="http://www.w3.org/1999/xhtml" src=""></video>
    </div>
    <div id="blue_control">
        Press <div class="blueButton"></div> to go back to the app
    </div>
    <div id="content">
        <div id="catalog">
            <?php
            /*
             * Leemos toda la información necesaria para crear el listado de todos los ítems que se encuentran
             * en el fichero.
             */
            $json_data = file_get_contents('data.json');
            $songs = json_decode($json_data);
            $i = 0;
            foreach($songs->data as $mydata) { ?>
                <div class="item <?php if($i === 0){ ?> active <?php  } ?>" num="<?php echo $i; ?>">
                    <div class="thumb">
                        <img src="img/Portada/<?php echo $mydata->thumb; ?>">
                    </div>
                    <div class="info">
                        <p class="titulo"><?php echo $mydata->title; ?></p>
                        <p class="artista"><?php echo $mydata->artist; ?></p>
                        <span><span><?php echo $mydata->votos; ?></span> votos</span>
                    </div>
                    <div class="botones">
                        <span class="up">Up</span>
                        <span>OK</span>
                        <span class="down">Down</span>
                    </div>
                </div>
            <?php $i++; }   ?>
        </div>
        <div id="preview">
            <div id="video">
                <video id="broadcast" xmlns="http://www.w3.org/1999/xhtml" src="./video/izal.mp4" autoplay="autoplay" loop="loop"></video>
            </div>
            <div id="info">
                <div id="album">
                    <p>Información de la canción</p>
                    <ul>
                        <li></li>
                        <li>Artista: <span></span></li>
                        <li>Album: <span></span></li>
                        <li><span></span> Votos</li>
                    </ul>
                </div>
                <div id="users">
                    <p>Usuarios Conectados</p>
                    <ul>
                        <?php $json_data = file_get_contents('users.json');
                        $users = json_decode($json_data);
                        for ($i = 0; $i < 3; $i++): ?>
                            <li><?php echo $users->data[$i]->name; ?></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div id="botones">
            <div class="red"><span></span>Play</div>
            <div class="green"><span></span>Pause</div>
            <div class="yellow"><span></span>Stop</div>
            <div class="blue"><span></span>Fullscreen</div>
        </div>
    </div>
</body>
<script>
    document.getElementById('broadcast').addEventListener('loadedmetadata', function() {
        this.currentTime = <?php echo $_GET['sec']; ?>;
    }, false);
</script>
</html>