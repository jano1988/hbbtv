var time = 0;
var interval;

/*
*
* Inicializamos HbbTV
* Creamos un bucle temporal para mostrar y ocultar mensajes para el usuario
* Activamos el manejador del teclado
*
 */
function hbbtvlib_red_initialize(){
  document.onkeydown = eventHandler;
  var appManager = document.getElementById("oipfAppMan").getOwnerApplication(document);
  appManager.show();
  interval = setInterval(homePage, 1000);
  appManager.privateData.keyset.setValue(0x2+0x4);
}

/*
*
* Gestionamos las acciones de los botones de HbbTV
* Cuando apretamos el botón rojo generamos un número aleatorio para la sincronización y paramos el bucle temporal
* que muestra y oculta los mensajes para el usuario
* Cuando apretamos el "OK" si aún no se ha apretado el botón rojo no pasa nada, si ya se ha apretado el botón rojo
* se cambia de pantalla
*
 */
function eventHandler(e) {
  switch (e.keyCode) {
    case KEY_RED:
      let syncNumber = Math.floor(1000 + Math.random() * 9000);
      clearInterval(interval);
      document.getElementById("red_control").style.display = "none";
      document.getElementById("numVar").innerHTML = syncNumber;
      document.getElementById("sync").style.display = "block";
      break;
    case KEY_OK:
      if(document.getElementById("sync").style.display === "block"){
          document.location.href = './app.php?sec='+document.getElementById('vid1').currentTime;
      }else{
        console.log('Otro botón');
      }
      break;
    default:
      console.log('Otro botón');
      break;
  }
}

/*
*
* Getiona el bucle temporal que oculta o muestra los mensajes para el usuario dependiendo de que segundo sea hace una
* cosa u otra
*
 */

function homePage(){
  time++;
  switch (time) {
    case 10:
    case 15:
    case 20:
    case 80:
      showHide("red_control");
      break;
    case 85:
      showHide("red_control");
      clearInterval(interval);
      break;
  }
}

/*
*
* Se encarga de ocultar o mostrar el mensaje. Dependiendo de su estado incial lo cambia al contrario
*
 */
function showHide(ID) {
  var x = document.getElementById(ID);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

/*
*
* Se encarga de reporducir o pausar el video. Dependiendo de su estado incial lo cambia al contrario.
*
 */
function playPause() {
  var myVideo = document.getElementById("video-player");
  myVideo.controls = false;
  if (myVideo.paused)
    myVideo.play();
  else
    myVideo.pause();
}