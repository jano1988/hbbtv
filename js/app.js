var interval;
/*
*
* Inicializamos HbbTV
* Activamos el manejador del teclado
*
 */
function hbbtvlib_app_initialize(){
    document.onkeydown = eventHandler;
    var appManager = document.getElementById("oipfAppMan").getOwnerApplication(document);
    appManager.show();
    appManager.privateData.keyset.setValue(0x2+0x4);
}

/*
*
* Según el índice del item seleccionado hacemos una consulta al archivo donde se encuentra toda la información y se
* coloca donde toca en el html
*
 */
function mostrarInfo(index){
    $.ajax({
        method: "POST",
        url: "showInfo.php",
        data: { index: index }
    }).done(function(data) {
        data = data.split('||');
        $('#preview #info #album ul li:first-child').text(data[0]);
        $('#preview #info #album ul li:nth-child(2) span').text(data[1]);
        $('#preview #info #album ul li:nth-child(3) span').text(data[2]);
        $('#preview #info #album ul li:nth-child(4) span').text(data[3]);
    });
}
/*
*
* Según el video que se quiere reproducir hacemos una consulta al archivo donde se encuentra toda la información y
* seleccionando el item que toca ponemos la url que toca en el elemento video que se va a reproducir en el html
*
 */
function getSrc(index){
    $.ajax({
        async: false,
        method: "POST",
        url: "getSRC.php",
        data: { index: index }
    }).done(function(data) {
        $("video#real").attr('num', $(".active").attr('num'));
        $("video#real").attr('src', './video/'+data);
    });
}

/*
*
* Gestionamos las acciones de los botones de HbbTV
* Cuando apretamos el botón rojo comienzamos a reproducir el video del item en el que estamos.
* Cuando apretamos el "OK" aumentamos un voto al item en el que estamos.
* Cuando apretamos el botón verde pausamos el video que se está reproduciendo
* Cuando apretamos el botón azul ponemos el video que se está reproduciendo en pantalla completa y en caso de que ya se
* esté reproduciendo en pantalla completa lo volvemos a poner al tamaño incial
* Cuando apretamos el botón amarillo se para el video que se está reproduciendo y se vuelve a reproducir el video por
* defecto de la aplicación.
* Cuando apretamos la flecha hacia abajo recorremos la lista de items bajando por ella.
* Cuando apretamos la flecha hacia arriba recorremos la lista de items subiendo por ella.
*
 */
function eventHandler(e) {
    switch (e.keyCode) {
        case KEY_RED:
            var vid = document.getElementById("broadcast");
            vid.muted = true;
            $("video#broadcast").hide();
            num = $(".active").attr('num');
            numV = $("video#real").attr('num');
            if(num != numV){
                $("video#real").show();
                getSrc($(".active").attr('num'));
                $("video#real").get(0).play();
            }else{
                $("video#real").get(0).play();
            }
            break;
        case KEY_OK:
           var listItem = $(".active");
            $.ajax({
                method: "POST",
                url: "addVote.php",
                data: { index: $(".active").attr('num') }
            }).done(function(data) {
                listItem.find('.info span span').text(data);
            });
            mostrarInfo($(".active").attr('num'));
            break;
        case KEY_GREEN:
            $("video#real").get(0).pause();
            break;
        case KEY_BLUE:
            var vid = document.getElementById("broadcast");
            var blue = document.getElementById("blue_control");
            if(vid.muted){
                if($("video#real").hasClass( "noFullscreen" )){
                    blue.style.display = "block";
                    setTimeout(function(){ blue.style.display = "none" }, 5000);
                    $("#fullscreen").css('background', 'black');
                    $("video#real").removeClass('noFullscreen');
                }else{
                    blue.style.display = "none";
                    $("#fullscreen").css('background', 'none');
                    $("video#real").addClass('noFullscreen');
                }
            }
            break;
        case KEY_YELLOW:
            var vid = document.getElementById("real");
            vid.currentTime = 0;
            $("video#real").attr('num', -1);
            $("video#real").get(0).pause();
            $("video#real").hide();
            $("video#broadcast").show();
            var vid = document.getElementById("broadcast");
            vid.muted = false;
            $("video#real").addClass('noFullscreen');
            break;
        case KEY_DOWN:
            var items = $(".item");
            var listItem = $(".active");
            var index = items.index( listItem);
            if(index == 6){
                $('#catalog').append(items.eq(0));
                listItem.removeClass('active');
                items.eq(index+1).addClass('active');
            }else{
                listItem.removeClass('active');
                items.eq(index+1).addClass('active');
            }
            mostrarInfo($(".active").attr('num'));
            break;
        case KEY_UP:
            var items = $(".item");
            var listItem = $(".active");
            var index = items.index( listItem);
            if(index == 0) {
                $('#catalog').prepend(items.eq(items.length-1));
                listItem.removeClass('active');
                items.eq(-1).addClass('active');
            }else{
                listItem.removeClass('active');
                items.eq(index-1).addClass('active');
            }
            mostrarInfo($(".active").attr('num'));
            break;
        case KEY_0:
            break;
        default:
            console.log('Otro botón');
            break;
    }
}