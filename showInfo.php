<?php

/*
 * Leemos el fichero donde tenemos toda la información y seleccionamos el item en la posición que nos indica la variable
 * index pasada como parámetro POST y devolvemos toda la información que se debe mostrar desde la aplicación.
 */

$json_data = file_get_contents('data.json');
$songs = json_decode($json_data);

echo $songs->data[$_POST['index']]->title."||".$songs->data[$_POST['index']]->artist."||".$songs->data[$_POST['index']]->album."||".$songs->data[$_POST['index']]->votos;

?>