<?php

/*
 * Leemos el fichero donde tenemos toda la información y seleccionamos el item en la posición que nos indica la variable
 * index pasada como parámetro POST y devolvemos la ruta en la cual se encuentra el video de este ítem.
 */

$json_data = file_get_contents('data.json');
$songs = json_decode($json_data);

echo $songs->data[$_POST['index']]->video;

?>